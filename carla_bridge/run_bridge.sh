#!/bin/bash

BRIDGE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${BRIDGE_DIR}/../../carla_venv/bin/activate
source ${BRIDGE_DIR}/setup.sh

python ${BRIDGE_DIR}/main.py