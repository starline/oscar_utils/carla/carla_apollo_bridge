#!/bin/bash

xhost +local:docker || true

docker run --gpus all \
            -ti --rm \
            -e "DISPLAY" \
            -e "QT_X11_NO_MITSHM=1" \
            -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
            -e XAUTHORITY \
           --net=host \
           --privileged \
           --name carla carlasim/carla:0.9.14 \
           ./CarlaUE4.sh -nosound -quality-level=Low -carla-rpc-port=2000
